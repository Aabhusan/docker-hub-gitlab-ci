#!/bin/bash
echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin
docker build -t node-mongo-test .
docker tag node-mongo-test aabhusan/node-mongo-test:v5
docker push aabhusan/node-mongo-test:v5
